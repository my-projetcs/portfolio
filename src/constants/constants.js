export const projects = [
  {
    title: "Socialmedia app",
    description:
      "I spearheaded the development of a robust full-stack social media app using MongoDB, Express, React, and Node.js. I architectured the backend with Node.js and Express, implemented RESTful API, and designed an efficient MongoDB database. I also crafted a user-friendly React frontend with real-time updates and responsive design. My work showcases technical expertise and a commitment to innovation in web development.",
    image: "/images/1.png",
    tags: ["Mongo", "Express", "React", "Node"],
    source: "https://github.com/ashishkandukuri/socialmedia",
    visit: "https://github.com/ashishkandukuri/socialmedia",
    id: 0,
  },
  {
    title: "ChatRooms",
    description:
      "I have successfully designed and developed a real-time chatroom app with Socket.io, Express, Node.js, and JavaScript, showcasing expertise in building responsive and interactive web applications for seamless real-time communication. Implemented Socket.io for instant messaging and utilized Express and Node.js for efficient server-side operations. Demonstrated a talent for crafting innovative, user-friendly online interaction solutions.",
    image: "/images/2.png",
    tags: ["Socket.io", "Node", "Express", "JavaScript"],
    source: "https://github.com/ashishkandukuri/chatRooms",
    visit: "https://github.com/ashishkandukuri/chatRooms",
    id: 1,
  },
  {
    title: "Algorithm Visualizer",
    description:
      "I have designed and developed an Algorithm Visualizer project utilizing React and JavaScript, featuring comprehensive sorting algorithms and the Sieve of Eratosthenes algorithm for visualization. This project showcases my proficiency in creating educational and interactive tools, enabling users to gain a deeper understanding of complex algorithms through visual representation. It reflects my commitment to enhancing problem-solving skills and fostering a strong foundation in algorithmic thinking.",
    image: "/images/3.png",
    tags: ["React", "Javascript"],
    source: "https://github.com/ashishkandukuri/algorithm-visualizer",
    visit: "https://algorithm-visualizer-blond.vercel.app/",
    id: 2,
  },
];

export const TimeLineData = [
  { year: 2020, text: "Joined IIT Bhubaneswar college" },
  { year: 2021, text: "Started exploring various technologies" },
  { year: 2022, text: "Worked on my own projects" },
  {
    year: 2023,
    text: "Took training internship at Zuitt technologies on Full-stack Development",
  },
];
