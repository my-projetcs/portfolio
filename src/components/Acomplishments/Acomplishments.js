import React from "react";

import {
  Section,
  SectionDivider,
  SectionTitle,
} from "../../styles/GlobalComponents";
import { Box, Boxes, BoxNum, BoxText } from "./AcomplishmentsStyles";

const data = [
  { text: "Awarded in Ramanujan Talent Test 2017" },
  { text: "Secured an AIR 3817 in JEE-Advanced 2020" },
];

const Acomplishments = () => (
  <Section>
    <SectionTitle>Personal Acheivements</SectionTitle>
    <Boxes>
      {data.map((card, ind) => (
        <Box key={ind}>
          <BoxNum>{card.number}</BoxNum>
          <BoxText>{card.text}</BoxText>
        </Box>
      ))}
    </Boxes>
  </Section>
);

export default Acomplishments;
