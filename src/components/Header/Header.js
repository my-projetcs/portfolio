import Link from "next/link";
import React from "react";
import { SiGeeksforgeeks, SiLeetcode, SiCodechef } from "react-icons/si";
import { DiCssdeck } from "react-icons/di";

import {
  Container,
  Div1,
  Div2,
  Div3,
  NavLink,
  SocialIcons,
  Span,
} from "./HeaderStyles";

const Header = () => (
  <Container>
    <Div1>
      <Link href="/">
        <a
          style={{
            display: "flex",
            alignItems: "center",
            color: "white",
            marginBottom: "20px",
          }}
        >
          <DiCssdeck size="3rem" />
          <Span>Portfolio</Span>
        </a>
      </Link>
    </Div1>

    <Div2>
      <li>
        <Link href="#projects">
          <NavLink>Projects</NavLink>
        </Link>
      </li>
      <li>
        <Link href="#tech">
          <NavLink>Technologies</NavLink>
        </Link>
      </li>
      <li>
        <Link href="#about">
          <NavLink>AboutMe</NavLink>
        </Link>
      </li>
    </Div2>

    <Div3>
      <SocialIcons href="https://auth.geeksforgeeks.org/user/ashishkandukuri3817">
        <SiGeeksforgeeks size="3rem" />
      </SocialIcons>
      <SocialIcons href="https://leetcode.com/ashish_3817/">
        <SiLeetcode size="3rem" />
      </SocialIcons>
      <SocialIcons href="https://www.codechef.com/users/ashish_3817">
        <SiCodechef size="3rem" />
      </SocialIcons>
    </Div3>
  </Container>
);

export default Header;
