import React from "react";

import {
  Section,
  SectionText,
  SectionTitle,
} from "../../styles/GlobalComponents";
import Button from "../../styles/GlobalComponents/Button";
import { LeftSection } from "./HeroStyles";

const Hero = (props) => (
  <Section row nopadding>
    <LeftSection>
      <SectionTitle main center>
        Welcome to <br />
        MY PORTFOLIO
      </SectionTitle>
      <SectionText>I'm a Full-Stack-Developer...</SectionText>
      <Button
        onClick={() =>
          (window.location =
            "https://www.linkedin.com/in/ashish-kandukuri-31365a213")
        }
      >
        Know More
      </Button>
    </LeftSection>
  </Section>
);

export default Hero;
