import React from "react";
import { SiGeeksforgeeks, SiLeetcode, SiCodechef } from "react-icons/si";

import { SocialIcons } from "../Header/HeaderStyles";
import {
  FooterWrapper,
  LinkColumn,
  LinkItem,
  LinkList,
  LinkTitle,
  SocialContainer,
} from "./FooterStyles";

const Footer = () => {
  return (
    <FooterWrapper>
      <LinkList>
        <LinkColumn>
          <LinkTitle>Call</LinkTitle>
          <LinkItem href="tel: +91 9014318598">+91 9014318598</LinkItem>
        </LinkColumn>
        <LinkColumn>
          <LinkTitle>E-mail</LinkTitle>
          <LinkItem href="mailto: 20ec01010@iitbbs.ac.in">
            20ec01010@iitbbs.ac.in
          </LinkItem>
        </LinkColumn>
      </LinkList>
      <SocialContainer>
        <SocialIcons href="https://auth.geeksforgeeks.org/user/ashishkandukuri3817">
          <SiGeeksforgeeks size="3rem" />
        </SocialIcons>
        <SocialIcons href="https://leetcode.com/ashish_3817/">
          <SiLeetcode size="3rem" />
        </SocialIcons>
        <SocialIcons href="https://www.codechef.com/users/ashish_3817">
          <SiCodechef size="3rem" />
        </SocialIcons>
      </SocialContainer>
    </FooterWrapper>
  );
};

export default Footer;
